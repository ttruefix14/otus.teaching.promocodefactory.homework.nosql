﻿using System;

namespace Otus.Teaching.Pcf.ReceivingFromPartner.Core.Domain
{
    public class 
        PartnerPromoCodeLimit
    {
        public Guid Id { get; set; }

        public Guid PartnerId { get; set; }

        public virtual Partner Partner { get; set; }
        
        public DateTimeOffset CreateDate { get; set; }

        public DateTimeOffset? CancelDate { get; set; }

        public DateTimeOffset EndDate { get; set; }

        public int Limit { get; set; }
    }
}