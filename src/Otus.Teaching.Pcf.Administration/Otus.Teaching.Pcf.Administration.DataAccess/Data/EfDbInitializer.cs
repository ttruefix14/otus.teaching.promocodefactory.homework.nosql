﻿using Microsoft.EntityFrameworkCore;
using MongoDB.Bson;
using MongoDB.EntityFrameworkCore.Extensions;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Administration.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;

        public EfDbInitializer(DataContext dataContext)
        {
            _dataContext = dataContext;
        }
        
        public void InitializeDb()
        {
            //_dataContext.Database.EnsureDeleted();
            //_dataContext.Database.EnsureCreated();
            
            if (_dataContext.Employees.Count() == 0)
            {
                _dataContext.AddRange(FakeDataFactory.Employees);
                //_dataContext.AddRange(FakeDataFactory.Roles);

                _dataContext.SaveChanges();
            }
        }
    }
}