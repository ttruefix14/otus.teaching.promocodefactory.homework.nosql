﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Integration
{
    public class PreferencesGateway
        : IPreferencesGateway
    {
        private readonly HttpClient _httpClient;

        public PreferencesGateway(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<List<Preference>> GetPreferences()
        {
            var response = await _httpClient.GetFromJsonAsync<List<Preference>>("api/v1/preferences");

            return response;
        }

        public async Task<List<Preference>> GetPreferencesByIds(List<Guid> preferenceIds)
        {
            var response = await _httpClient.PostAsJsonAsync("api/v1/preferences/filter", preferenceIds);

            return await response.Content.ReadFromJsonAsync<List<Preference>>();
        }

        public async Task<List<Guid>> GetCustomersByPreferenceId(Guid preferenceId)
        {
            var response = await _httpClient.GetAsync($"api/v1/preferences/customers/{preferenceId}");

            try
            {
                if (response.StatusCode == System.Net.HttpStatusCode.NotFound)
                    throw new Exception($"Предпочтение не найдено");

                return await response.Content.ReadFromJsonAsync<List<Guid>>();
            }
            catch
            {
                throw;
            }
        }

        public async Task<List<Preference>> GetCustomerPreferences(Guid customerId)
        {
            var response = await _httpClient.GetFromJsonAsync<List<Preference>>($"api/v1/preferences/{customerId}");

            return response;
        }

        public async Task ReplaceCustomerPreferences(Guid customerId, List<Guid> preferences)
        {
            await _httpClient.PostAsJsonAsync($"api/v1/preferences/{customerId}", preferences);
        }

        public async Task DeleteCustomerPreferences(Guid customerId)
        {
            await _httpClient.DeleteAsync($"api/v1/preferences/{customerId}");
        }
    }
}