﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways
{
    public interface IPreferencesGateway
    {
        Task<List<Preference>> GetPreferences();

        Task<List<Preference>> GetPreferencesByIds(List<Guid> preferenceIds);

        Task<List<Preference>> GetCustomerPreferences(Guid customerId);

        Task<List<Guid>> GetCustomersByPreferenceId(Guid preferenceId);

        Task ReplaceCustomerPreferences(Guid customerId, List<Guid> preferences);

        Task DeleteCustomerPreferences(Guid customerId);
    }
}
