﻿using Otus.Teaching.Pcf.GivingToCustomer.Core.Abstractions.Gateways;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class EfDbInitializer
        : IDbInitializer
    {
        private readonly DataContext _dataContext;
        private readonly IPreferencesGateway _preferencesGateway;

        public EfDbInitializer(DataContext dataContext, IPreferencesGateway preferencesGateway)
        {
            _dataContext = dataContext;
            _preferencesGateway = preferencesGateway;
        }
        
        public void InitializeDb()
        {
            _dataContext.Database.EnsureDeleted();
            _dataContext.Database.EnsureCreated();
            
            _dataContext.AddRange(FakeDataFactory.Customers);
            _dataContext.SaveChanges();

            _preferencesGateway.ReplaceCustomerPreferences(FakeDataFactory.Customers.FirstOrDefault().Id, 
                FakeDataFactory.PreferenceIds);
        }
    }
}