﻿namespace Otus.Teaching.Pcf.Preferences.WebHost.Domain
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
    }
}