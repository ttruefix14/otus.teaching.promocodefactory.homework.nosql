﻿using System;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Domain
{
    public class CustomerPreference
    {
        public Guid CustomerId { get; set; }

        public Guid PreferenceId { get; set; }

        public virtual Preference Preference { get; set; }
    }
}
