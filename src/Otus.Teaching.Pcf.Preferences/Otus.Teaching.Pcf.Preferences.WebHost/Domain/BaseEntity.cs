﻿using System;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Domain
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}