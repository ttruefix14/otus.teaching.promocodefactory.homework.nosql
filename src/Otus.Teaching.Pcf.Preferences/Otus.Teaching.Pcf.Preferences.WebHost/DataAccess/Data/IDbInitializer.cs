﻿namespace Otus.Teaching.Pcf.Preferences.WebHost.DataAccess.Data

{
    public interface IDbInitializer
    {
        void InitializeDb();
    }
}