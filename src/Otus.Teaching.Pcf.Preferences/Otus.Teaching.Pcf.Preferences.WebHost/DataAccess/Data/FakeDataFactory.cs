﻿using Otus.Teaching.Pcf.Preferences.WebHost.Domain;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.Pcf.Preferences.WebHost.DataAccess.Data
{
    public static class FakeDataFactory
    {
        public static List<Preference> Preferences => new List<Preference>()
        {
            new Preference()
            {
                Id = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c"),
                Name = "Театр",
            },
            new Preference()
            {
                Id = Guid.Parse("c4bda62e-fc74-4256-a956-4760b3858cbd"),
                Name = "Семья",
            },
            new Preference()
            {
                Id = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84"),
                Name = "Дети",
            }
        };

        public static List<CustomerPreference> CustomerPreferences => new List<CustomerPreference>()
        {
            new CustomerPreference()
            {
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PreferenceId = Guid.Parse("76324c47-68d2-472d-abb8-33cfa8cc0c84")
            },
            new CustomerPreference()
            {
                CustomerId = Guid.Parse("a6c8c6b1-4349-45b0-ab31-244740aaf0f0"),
                PreferenceId = Guid.Parse("ef7f299f-92d7-459f-896e-078ed53ef99c")
            }
        };
    }
}