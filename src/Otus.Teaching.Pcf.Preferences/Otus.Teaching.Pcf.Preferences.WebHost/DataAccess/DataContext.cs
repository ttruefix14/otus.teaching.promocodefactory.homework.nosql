﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.Pcf.Preferences.WebHost.Domain;

namespace Otus.Teaching.Pcf.Preferences.WebHost.DataAccess
{
    public class DataContext
        : DbContext
    {

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        public DataContext()
        {

        }

        public DataContext(DbContextOptions<DataContext> options)
            : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CustomerPreference>().HasKey(x => new { x.CustomerId, x.PreferenceId });

            modelBuilder.Entity<CustomerPreference>().HasOne(x => x.Preference).WithMany();
        }
    }
}