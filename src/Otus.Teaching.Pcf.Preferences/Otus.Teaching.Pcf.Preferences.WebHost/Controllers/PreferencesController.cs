using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Distributed;
using Otus.Teaching.Pcf.Preferences.WebHost.Caching;
using Otus.Teaching.Pcf.Preferences.WebHost.DataAccess;
using Otus.Teaching.Pcf.Preferences.WebHost.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Controllers
{
    /// <summary>
    /// Предпочтения клиентов
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PreferencesController : ControllerBase
    {
        private readonly DataContext _dataContext;
        private readonly IDistributedCache _cache;
        private const string PREFERENCE_KEY = "Preferences";
        private const string CUSTOMER_KEY = "Customers";
        private string _preferenceKey(Guid id) => $"Customers_By_PreferenceId_{id}";
        private string _customerKey(Guid id) => $"Preferences_By_CustomerId_{id}";


        public PreferencesController(DataContext dataContext, IDistributedCache cache)
        {
            _dataContext = dataContext;
            _cache = cache;
        }

        /// <summary>
        /// Получить список предпочтений
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<Preference>>> GetPreferences()
        {
            var preferences = await _cache.GetAsync<List<Preference>>(PREFERENCE_KEY);

            if (preferences == null)
            {
                preferences = await _dataContext.Set<Preference>()
                    .AsNoTracking()
                    .ToListAsync();

                await _cache.SetAsync(PREFERENCE_KEY, preferences, slidingExpiration: 30);
            }

            return Ok(preferences);
        }

        /// <summary>
        /// Получить список предпочтений по идентификаторам
        /// </summary>
        /// <returns></returns>
        [HttpPost("filter")]
        public async Task<ActionResult<List<Preference>>> GetPreferencesByIds(List<Guid> preferenceIds)
        {

            var preferences = await _dataContext.Set<Preference>()
                .Where(x => preferenceIds.Contains(x.Id))
                .ToListAsync();

            if (preferences.Count != preferenceIds.Count)
                return NotFound($"Предпочтения: {string.Join(", ",
                    preferenceIds.Where(
                        x => !preferences.Select(x => x.Id).Contains(x)))} не найдены");

            return Ok(preferences);
        }

        /// <summary>
        /// Получить список предпочтений клиента
        /// </summary>
        /// <param name="id">id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<List<Preference>>> GetCustomerPreferences(Guid id)
        {
            var key = _customerKey(id);
            var preferences = await _cache.GetAsync<List<Preference>>(key);

            if (preferences == null)
            {
                preferences = await _dataContext.Set<CustomerPreference>()
                    .AsNoTracking()
                    .Where(x => x.CustomerId == id)
                    .Include(x => x.Preference)
                    .Select(x => new Preference() { Id = x.PreferenceId, Name = x.Preference.Name })
                    .ToListAsync();

                await _cache.SetAsync(key, preferences, slidingExpiration: 30);
            }

            return Ok(preferences);
        }

        /// <summary>
        /// Получить список клиентов по предпочтению
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("customers/{id:guid}")]
        public async Task<ActionResult<List<Guid>>> GetCustomersByPreferenceId(Guid id)
        {
            var key = _preferenceKey(id);
            var customers = await _cache.GetAsync<List<Guid>>(key);

            if (customers == null)
            {
                if (await _dataContext.Set<Preference>().FindAsync(id) == null)
                    return NotFound($"Предпочтения с id: {id} не существует");

                customers = await _dataContext.Set<CustomerPreference>()
                    .AsNoTracking()
                    .Where(x => x.PreferenceId == id)
                    .Select(x => x.CustomerId)
                    .ToListAsync();

                await _cache.SetAsync(key, customers, slidingExpiration: 30);
            }
            ;
            return Ok(customers);
        }

        /// <summary>
        /// Создать или заменить предпочтения клиента
        /// </summary>
        /// <param name="id">id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="preferenceIds"></param>
        /// <returns></returns>
        [HttpPost("{id:guid}")]
        public async Task<IActionResult> ReplaceCustomerPreferences(Guid id, List<Guid> preferenceIds)
        {
            var preferences = await _dataContext.Set<Preference>()
                .Where(x => preferenceIds.Contains(x.Id))
                .ToListAsync();

            if (preferences.Count != preferenceIds.Count)
                return NotFound($"Предпочтения: {string.Join(", ",
                    preferenceIds.Where(
                        x => !preferences.Select(x => x.Id).Contains(x)))} не найдены");

            var customerPreferencesToRemove = _dataContext.Set<CustomerPreference>()
                .Where(x => x.CustomerId == id);
            _dataContext.RemoveRange(customerPreferencesToRemove);

            await _dataContext.AddRangeAsync(
                preferenceIds.Select(x => new CustomerPreference { CustomerId = id, PreferenceId = x }));

            await _dataContext.SaveChangesAsync();

            await _cache.RemoveAsync(_customerKey(id));

            return Ok();
        }

        /// <summary>
        /// Удалить предпочтения клиента
        /// </summary>
        /// <param name="id">id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteCustomerPreferences(Guid id)
        {
            var customerPreferencesToRemove = _dataContext.Set<CustomerPreference>()
                .Where(x => x.CustomerId == id);
            _dataContext.RemoveRange(customerPreferencesToRemove);
            await _dataContext.SaveChangesAsync();

            await _cache.RemoveAsync(_customerKey(id));

            return Ok();

        }

        /// <summary>
        /// Удалить предпочтения клиента по идентификаторам
        /// </summary>
        /// <param name="id">id клиента, например <example>a6c8c6b1-4349-45b0-ab31-244740aaf0f0</example></param>
        /// <param name="preferenceIds"></param>
        /// <returns></returns>
        [HttpDelete("filter/{id:guid}")]
        public async Task<IActionResult> DeleteCustomerPreferencesByIds(Guid id, List<Guid> preferenceIds)
        {
            var customerPreferencesToRemove = _dataContext.Set<CustomerPreference>()
                            .Where(x => preferenceIds.Contains(x.PreferenceId));

            _dataContext.RemoveRange(customerPreferencesToRemove);
            await _dataContext.SaveChangesAsync();

            await _cache.RemoveAsync(_customerKey(id));

            return Ok();
        }
    }
}
