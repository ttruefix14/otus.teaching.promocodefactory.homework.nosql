﻿using Microsoft.Extensions.Caching.Distributed;
using System;
using System.Text.Json;
using System.Threading.Tasks;

namespace Otus.Teaching.Pcf.Preferences.WebHost.Caching
{
    public static class CachingExtensions
    {
        public static async Task<T> GetAsync<T>(this IDistributedCache cache, string key)
        {
            var cached = await cache.GetStringAsync(key);

            if (string.IsNullOrEmpty(cached))
                return default;

            return JsonSerializer.Deserialize<T>(cached);
        }

        public static async Task SetAsync<T>(
            this IDistributedCache cache,
            string key,
            T value,
            double? absoluteExpirationRelativeToNow = null,
            double? slidingExpiration = null)
        {
            DistributedCacheEntryOptions cacheEntryOptions = new DistributedCacheEntryOptions
            {
                // Remove item from cache after duration
                AbsoluteExpirationRelativeToNow = absoluteExpirationRelativeToNow.HasValue 
                                ? TimeSpan.FromSeconds(absoluteExpirationRelativeToNow.Value) 
                                : null,

                // Remove item from cache if unused for the duration
                SlidingExpiration = slidingExpiration.HasValue
                                ? TimeSpan.FromSeconds(slidingExpiration.Value)
                                : null
            };
            
            await cache.SetStringAsync(key,
                JsonSerializer.Serialize(value),
                cacheEntryOptions);
        }
    }
}
