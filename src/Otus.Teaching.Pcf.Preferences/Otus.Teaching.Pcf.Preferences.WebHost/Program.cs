using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.Pcf.Preferences.WebHost.DataAccess;
using Otus.Teaching.Pcf.Preferences.WebHost.DataAccess.Data;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddControllers().AddMvcOptions(x =>
    x.SuppressAsyncSuffixInActionNames = false);
builder.Services.AddScoped<IDbInitializer, EfDbInitializer>();

builder.Services.AddDbContext<DataContext>(x =>
{
    //x.UseSqlite("Filename=PromocodeFactoryReceivingFromPartnerDb.sqlite");
    x.UseNpgsql(builder.Configuration.GetConnectionString("PromocodeFactoryPreferencesDb"));
    x.UseSnakeCaseNamingConvention();
});
builder.Services.AddStackExchangeRedisCache(options =>
{
    options.Configuration = builder.Configuration.GetConnectionString("Redis");
    options.InstanceName = "PromocodeFactoryPreferences_";
});

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
//builder.Services.AddEndpointsApiExplorer();
builder.Services.AddOpenApiDocument(options =>
{
    options.Title = "PromoCode Factory Receiving From Partner API Doc";
    options.Version = "1.0";
});

var app = builder.Build();

if (app.Environment.IsDevelopment())
{
    app.UseDeveloperExceptionPage();
}
else
{
    app.UseHsts();
}

app.UseOpenApi();
app.UseSwaggerUi(x =>
{
    x.DocExpansion = "list";
});

app.UseHttpsRedirection();

app.UseRouting();

app.MapControllers();

app.Services.CreateScope().ServiceProvider.GetRequiredService<IDbInitializer>().InitializeDb();

app.Run();